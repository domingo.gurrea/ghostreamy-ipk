# ghostreamy-ipk

Ghostreamy Panel IPTV Streaming Enigma2

Ghostreamy es un panel de streaming para enigma2. Aquí puedes encontrar los paquetes ipk para las distintas arquitecturas.

### FEATURES

- Streaming https
- Control de clientes
- Gestión de certificados con Duckdns y Let'sEncrypt
- Compatibilidad con aplicaciones IPTV
- Emulación HDHomeRun
- [Más...](http://tropical.jungle-team.online/ghostreamy/ghostreamy.html)

### DOCUMENTACION
#### [Información](http://tropical.jungle-team.online/ghostreamy/ghostreamy.html)
#### [Presentación](https://jungle-team.com/ghostreamy-panel-stream-enigma2/)
#### [Manual Básico](https://jungle-team.com/ghostreamy-manual-dummy/)

